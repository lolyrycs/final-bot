module.exports = function(controller) {

    controller.hears(['Cosa fa VEM','Cosa fa vem','Cosa fa VEM?','Cosa fa vem?','Chi è vem','Chi è VEM','Chi è vem?','Chi è VEM?','In che settore opera vem', 'In che aree opera vem','Aree operative di vem','Quali sono le aree operative di vem'], 'message_received', function(bot, message) {
        bot.reply(message,"E' un'azienda operatrice nel settore infrastrutturale delle reti informatiche e al suo interno è divisa in due sezioni: una si occupa dello sviluppo software, l'altra della sicurezza.");
    });

    controller.hears(['Quanti clienti ha VEM?','Quanti clienti ha vem?','Quanti clienti ha VEM','Quanti clienti ha vem','Quanti sono i clienti di vem','Quanti sono i clienti di VEM','Quanti sono i clienti di vem?','Quanti sono i clienti di VEM?','Quali aziende sono clienti di vem'], 'message_received', function(bot,message) {
 		bot.reply(message,'Circa 800, tra i quali: \n - Cisco \n - Microsoft \n - Intel \n - HP \n e molti altri!');
	});

 	controller.hears(['In cosa può essere utile vem','A cosa può essermi utile vem?','A cosa può essere utile VEM?','A cosa serve vem','In cosa è specializzata vem','Di cosa si occupa vem','Cosa fa vem','Di quali settori si occupa vem','In che ambiti opera vem','In che ambiti lavora vem'], 'message_received', function(bot,message) {
 		bot.reply(message,"VEM è specializzata in \n - Networking, \n - Data Center, \n - Security \n - Cloud Technologies.");
	});

    controller.hears(['Quali sono le tecnologie più utilizzate','Quali sono le tecnologie più utilizzate da vem','Quali sono le tecnologie utilizzate','Quali sono le tecnologie utilizzate da vem','Quali tecnologie utilizza vem','Quali tecnologie utilizza','Quali sono gli strumenti usati','Quali sono gli strumenti più usati','Quali sono gli strumenti usati da vem', 'Quali sono gli strumenti principali di vem','Quali sono gli strumenti principali usati da vem', 'Quali sono gli strumenti principali utilizzati da vem'], 'message_received', function(bot,message) {
 		bot.reply(message,'Tecnologie Cloud: Mobile, Ufield computing, Storage.');
	});

    controller.hears(['Che cosa è myvem','Cosa è myVEM','Spiegami cosa è myvem','Informazioni su myvem','Che vantaggi ha myvem','A cosa serve myvem'], 'message_received', function(bot,message) {
 		bot.reply(message,'MyVEM è un’area esclusivamente dedicata ai clienti per la gestione di servizi e richieste erogati in modalità Cloud.');
	});

 	controller.hears(['Quali sono le aziende del gruppo vem','Quali aziende fanno parte del gruppo vem','Quali aziende fanno parte di vem','Quali aziende sono parte di vem','Quali aziende sono parte di vem','Quali sono le aziende di vem','Quali aziende appartengono a vem','Quali aziende sono appartenenti a vem'], 'message_received', function(bot,message) {
 		bot.reply(message,"Alcune tra le aziende appartenenti a vem: \n - MyDev \n - Certego \n - Vem ");
	});

 	controller.hears(['Chi posso contattare','Contatti','Mi serve un contatto','Come posso contattare vem','Come contatto vem','Contatto','Chi posso contattare','Voglio un contatto'], 'message_received', function(bot,message) {
 		bot.reply(message,"VEM è presente con 7 sedi su tutto il territorio italiano. Puoi contattarci tramite modulo online, email o telefono. ");
 		bot.reply(message, "Forlì Via degli Scavi 36, 47122 Forlì (FC) \n - Email: info@vem.com \n - Cellulare: +39 0543 725005 \n - Fisso: +39 0543 725277 <br><br><a href='https://vem.com/contatti/' target='_blank'>Visita pagina nel sito</a>");
	});

 	controller.hears(['Dove opera vem','Dove sono le sedi di vem','Dove si trovano le sedi vem','Come posso contattare vem','Come contatto vem','Contatto','Chi posso contattare','Voglio un contatto'], 'message_received', function(bot,message) {
 		bot.reply(message,"VEM è presente con 7 sedi su tutto il territorio italiano. \n- Forlì Via degli Scavi 36 \n- Forlì NOC/SOC Via Cardano 8 \n- Modena Via G. Perlasca 25 \n- Padova Via San Marco 9/H \n- Senigallia Via D. Corvi 8 \n- Vimercate Palazzo Larice, via Torri Bianche 3 \n- Roma Viale L. Gaurico 9/11, scala C");

	});

 	controller.hears(['Eventi','Eventi recenti','Mostrami eventi recenti','Mostrami gli eventi','Mostrami eventi'], 'message_received', function(bot,message) {
 		bot.reply(message,"Visita la pagina eventi nel nostro sito: <br><br><a href='https://vem.com/rivivi-i-nostri-eventi/' target='_blank'>Visita pagina nel sito</a>");
	});

 	controller.hears(['Help','Aiuto','Aiutami'], 'message_received', function(bot,message) {
 		bot.reply(message, {
        text: "Seleziona un'opzione: ",
        quick_replies: [
            {
                title: 'Chi sei tu?',
                payload: 'Chi sei tu?'
            },
            {
                title: 'Informazioni su VEM',
                payload: 'Informazioni su VEM'
            },
            {
            	title: 'Come vengono gestiti i miei dati?',
            	payload: 'Come vengono gestiti i miei dati?'
            },
        ]
      }, function() {});
	});

	controller.hears('Chi sei tu?', 'message_received', function(bot,message) {
 		bot.reply(message,"Io sono il bot automatico di VEM, sono attivo 24/24, 365 giorni l'anno. Fornisco informazioni su VEM, e sui servizi degli utenti collegati a MyVEM, in modo del tutto automatico!");

	});

	controller.hears('Informazioni su VEM','message_received', function(bot,message) {
 		bot.reply(message, {
        text: "Seleziona un'opzione: ",
        quick_replies: [
            {
                title: 'Cosa fa VEM?',
                payload: 'Cosa fa VEM?'
            },
            {
                title: 'Quanti clienti ha VEM?',
                payload: 'Quanti clienti ha VEM?'
            },
            {
            	title: 'Di cosa si occupa vem?',
            	payload: 'Di cosa si occupa vem?'
            },
            {
            	title: 'Quali tecnologie utilizza vem?',
            	payload: 'Quali tecnologie utilizza vem?'
            },
            {
                title: 'Cosa è myVEM?',
                payload: 'Cosa è myVEM?'
            },
            {
                title: 'Quali sono le aziende del gruppo vem?',
                payload: 'Quali sono le aziende del gruppo vem?'
            },
            {
                title: 'Contatti',
                payload: 'Contatti'
            },
            {
                title: 'Eventi',
                payload: 'Eventi'
            },
        ]
      }, function() {});

	});

	controller.hears('Come vengono gestiti i miei dati?', 'message_received', function(bot,message) {
 		bot.reply(message,"Tutti i tuoi dati sono conservati e protetti da VEM tramite la sua rete, con la sicurezza al primo posto. Tutti i dati riguardanti i servizi MyVEM sono salvati nei nostri database e accessibili tramite il portale MyVEM. Tutte le tue conversazioni e i messaggi inviati al VEM Support Bot sono visibili in questa chat e sono eliminati 30 giorni dopo l'invio.");
	});
	
	/*controller.hears('Chi è gabriele', 'message_received', function(bot,message) {
 		bot.reply(message,"E' quel pirla accanto a te :)");
	});


	/*  //Dovrebbe controllare le risposte che non conosce, ma non funzia
	controller.on('direct_mention', function(bot, message) {
		bot.say({text: 'Non conosco questa domanda o non hai i permessi per accedere alla risorsa.'});
	});
	*/
}
