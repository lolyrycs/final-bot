var request = require('request');

module.exports = function(controller) {
/*	controller.hears(['info vem','Che cosa fa VEM','Cosa fa VEM','Che cosa fa vem','Cosa fa vem'],'message_received', function(bot, message) {

		bot.startConversation(message, function(err, convo) {

			convo.say("E' un'azienda operatrice nel settore infrastrutturale delle reti informatiche e al suo interni è divisa in due sezioni: una si occupa dello sviluppo software, l'altra della sicurezza.");

		});
	});

	controller.hears(['clienti vem','Quanti clienti ha VEM'],'message_received', function(bot, message) {
		bot.startConversation(message, function(err, convo) {
			convo.say("Circa 800");
		});
	});

	controller.hears(['utilità vem','In cosa può essere utile VEM','in cosa può essermi utile vem'],'message_received', function(bot, message) {

		bot.startConversation(message, function(err, convo) {

			convo.say("E' specializzata in Networking e Data Center.");

			convo.say({
				text: 'Sei interessato ai prodotti VEM o vorresti diventare un collaboratore?',
				//elenco delle risposte rapide
				quick_replies: [
				  {
					title: 'Collaboratore',
					payload: '',
				  },
				  {
					title: 'Prodotti',
					payload: ''
				  }
				]
			});
		});
    });

	controller.hears(['contatti','contattami'],'message_received', function(bot, message) {
		bot.startConversation(message, function(err, convo) {
			convo.say("[Qui](https://vem.com/contatti/) puoi trovare i nostri contatti");
		});
	});*/

	//request API
	controller.hears(['api'], 'message_received', function(bot, message) {
		var member ='';
		let c=0;
		bot.startConversation(message, function(err, convo) {
			request('http://localhost:5000/api/TeamCore', function (err, response, body) {
				console.log('error: ', err); // Handle the error if one occurred
				console.log('statusCode: ', response && response.statusCode); // Check 200 or such
				convo.say('This is the list of users: ');
				c=JSON.parse(body);
				for(var i=0; i<c.TeamCore.length; i++){
					member=c.TeamCore[i];
					convo.say('Name: '+member.nome+'		Surname: '+member.cognome);
				}
			})
		})
	});

}
