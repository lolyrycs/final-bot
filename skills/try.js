//importo il contoller (Core del Bot)
module.exports = function(controller) {
	
	controller.hears(['question me'], 'message_received', function(bot,message) {

	  // start a conversation to handle this response.
	  bot.startConversation(message,function(err,convo) {

		convo.addQuestion('How are you?',function(response,convo) {

		  convo.say('Cool, you said: ' + response.text);
		  convo.next();

		},{},'default');

	  })

	});
		
	//sta in ascolto del messaggio "Lorenzo"
	controller.hears('Lorenzo','message_received', function(bot, message) {
		
		//avvia la conversazione
		bot.startConversation(message, function(err, convo) {
			
			//convo(conversazione) risponde con un messaggio semplice
			convo.say("Ciao bell' uomo");
			
			//Domanda
			convo.say({
				text: 'Vuoi un caffè?',
				//elenco delle risposte rapide
				quick_replies: [
				  {
					//testo del bottone 
					title: 'Una shweppes',
					//trigger del messaggio "prova"
					payload : 'prova',
				  },
				  {
					title: 'Si',
					payload: '',
				  },
				  {
					title: 'no',
					payload: ''
				  }
				]
			});
		});
    });
}